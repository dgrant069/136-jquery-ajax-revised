# RailsCasts Example Application

Run these commands to try it out.

```
bundle
rake db:setup
rails s
```

Requires Ruby ~> 1.9.2

# Now app will preform the CRUD within task index without redirects.
Using AJAX, this app will create, update the status (comp./incomp.), delete.
All from within one view
